﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnFinishTrigger : MonoBehaviour {

    Rigidbody2D r2d = new Rigidbody2D();
	
	// Update is called once per frame
	void WordlsCollide (Collision2D col) {
		if (col.gameObject.tag == "Player")
        {
            Destroy(col.gameObject);
        }
	}
}
